<?php
/*
 * Plugin Name: itbh katt
 * Version: 1.0
 * Plugin URI: http://www.hughlashbrooke.com/
 * Description: This is your starter template for your next WordPress plugin.
 * Author: Hugh Lashbrooke
 * Author URI: http://www.hughlashbrooke.com/
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: itbh-katt
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Hugh Lashbrooke
 * @since 1.0.0
 */


if ( ! defined( 'ABSPATH' ) ) exit;

// Load plugin class files
require_once( 'includes/class-itbh-katt.php' );
require_once( 'includes/class-itbh-katt-settings.php' );
require_once( 'includes/class-itbh-page-template.php' );

// Load plugin libraries

require_once( 'includes/lib/class-itbh-katt-post-type.php' );
require_once( 'includes/lib/class-itbh-katt-taxonomy.php' );
require_once( 'includes/admin/class-itbh-katt-admin-api.php' );

/**
 * Returns the main instance of itbh_katt to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object itbh_katt
 */
function itbh_katt () {
	$instance = itbh_katt::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = itbh_katt_Settings::instance( $instance );
	}

	return $instance;
}

$GLOBALS['itbh_katt'] = itbh_katt();
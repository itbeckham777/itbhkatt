<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 1/8/2017
 * Time: 3:45 AM
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

include_once(itbh_katt()->dir . '/vendor/autoload.php');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

// Create the logger
global $logger;
$logger = new Logger('my_logger');
// Now add some handlers
$logger->pushHandler(new StreamHandler('c:/logs/out.log', Logger::DEBUG));
$logger->pushHandler(new FirePHPHandler());

/**
 * WC_Admin_Meta_Boxes.
 */
class itbh_katt_Admin_Meta_Boxes {

    private static $saved_meta_boxes = false;
    public static $meta_box_errors  = array();

    public function __construct() {
        add_filter( 'itbh_3d_model_custom_fields', array( $this, 'itbh_3d_model_fields' ) );

        add_action( 'add_meta_boxes', array( $this, 'remove_meta_boxes' ), 10 );
        add_action( 'add_meta_boxes', array( $this, 'rename_meta_boxes' ), 20 );
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );


    }

    /**
     * Add an error message.
     * @param string $text
     */
    public static function add_error( $text ) {
        self::$meta_box_errors[] = $text;
    }

    /**
     * Save errors to an option.
     */
    public function save_errors() {
        update_option( 'itbh_meta_box_errors', self::$meta_box_errors );
    }

    /**
     * Show any stored error messages.
     */
    public function output_errors() {
        $errors = maybe_unserialize( get_option( 'itbh_meta_box_errors' ) );

        if ( ! empty( $errors ) ) {

            echo '<div id="itbh_errors" class="error notice is-dismissible">';

            foreach ( $errors as $error ) {
                echo '<p>' . wp_kses_post( $error ) . '</p>';
            }

            echo '</div>';

            // Clear
            delete_option( 'itbh_meta_box_errors' );
        }
    }

    /**
     * Add Meta boxes.
     */
    public function add_meta_boxes() {
        global $itbh_katt;
        $itbh_katt->admin->add_meta_box( 'itbh-3d-model-data', '3D Model Data', 'itbh_3d_model' );
//        add_meta_box( 'itbh-3d-model-data', '3D Model Data', 'itbh_katt_Meta_Box_Product_Data::output', 'itbh_3d_model', 'normal', 'high' );


        // We're including the farbtastic script & styles here because they're needed for the colour picker
        // If you're not including a colour picker field then you can leave these calls out as well as the farbtastic dependency for the wpt-admin-js script below
        wp_enqueue_style( 'farbtastic' );
        wp_enqueue_script( 'farbtastic' );

        // We're including the WP media scripts here because they're needed for the image upload field
        // If you're not including an image upload then you can leave this function call out
        wp_enqueue_media();

        wp_register_script( $itbh_katt->_token . '-settings-js', $itbh_katt->assets_url . 'js/settings' . $itbh_katt->script_suffix . '.js', array( 'farbtastic', 'jquery' ), '1.0.0' );
        wp_enqueue_script( $itbh_katt->_token . '-settings-js' );


    }


    /**
     * Remove bloat.
     */
    public function remove_meta_boxes() {
        remove_meta_box( 'postexcerpt', 'itbh_3d_model', 'normal' );
        remove_meta_box( 'pageparentdiv', 'itbh_3d_model', 'side' );
        remove_meta_box( 'commentsdiv', 'itbh_3d_model', 'normal' );
        remove_meta_box( 'commentstatusdiv', 'itbh_3d_model', 'side' );
        remove_meta_box( 'commentstatusdiv', 'itbh_3d_model', 'normal' );
    }

    /**
     * Rename core meta boxes.
     */
    public function rename_meta_boxes() {
//        global $post;

        // Comments/Reviews
//        if ( isset( $post ) && ( 'publish' == $post->post_status || 'private' == $post->post_status ) ) {
//            remove_meta_box( 'commentsdiv', 'product', 'normal' );
//
//            add_meta_box( 'commentsdiv', __( 'Reviews', 'itbh' ), 'post_comment_meta_box', 'product', 'normal' );
//        }
    }


    public function itbh_3d_model_fields(){
        $fields = array(
            array(
                'id' 			=> 'itbh_katt_model_name',
                'label'			=> __( 'Model Name' , 'itbh-katt' ),
                'description'	=> __( 'This is the 3d model name.', 'itbh-katt' ),
                'type'			=> 'text',
                'default'		=> '',
                'placeholder'	=> __( '', 'itbh-katt' ),
                'metabox'       => 'itbh-3d-model-data'
            ),
            array(
                'id' 			=> 'itbh_katt_model_file',
                'label'			=> __( 'Model File' , 'itbh-katt' ),
                'description'	=> __( 'This is the 3d model file.', 'itbh-katt' ),
                'type'			=> 'file',
                'default'		=> '',
                'placeholder'	=> '',
                'metabox'       => 'itbh-3d-model-data'
            ),
            array(
                'id' 			=> 'itbh_katt_model_image',
                'label'			=> __( 'Model Image' , 'itbh-katt' ),
                'description'	=> __( 'This is the 3d model image.', 'itbh-katt' ),
                'type'			=> 'image',
                'default'		=> '',
                'placeholder'	=> '',
                'metabox'       => 'itbh-3d-model-data'
            )
        );

        return $fields;
    }

}

new itbh_katt_Admin_Meta_Boxes();
<?php
/**
 * Product Data
 *
 * Displays the product data box, tabbed, with several panels covering price, stock etc.
 *
 * @author   WooThemes
 * @category Admin
 * @package  WooCommerce/Admin/Meta Boxes
 * @version  2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

include_once(itbh_katt()->dir . '/vendor/autoload.php');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

// Create the logger
global $logger;
$logger = new Logger('my_logger');
// Now add some handlers
$logger->pushHandler(new StreamHandler('c:/logs/out.log', Logger::DEBUG));
$logger->pushHandler(new FirePHPHandler());

$logger->addInfo( 'meta box file start' );

class itbh_katt_Meta_Box_Product_Data {

    /**
     * Output the metabox.
     *
     * @param WP_Post $post
     */
    public static function output( $post ) {
        global $post, $thepostid;
        global $itbh_katt;

        $itbh_katt->admin->
        wp_nonce_field( 'itbh_save_data', 'itbh_meta_nonce' );

        $thepostid = $post->ID;
        ?>
        <table>
            <tr>
                <td>Model Name</td>
                <td><input type="text" name="itbh-katt-model-name" value="<?= get_post_meta($thepostid, 'itbh-katt-model-name', true)?>"></td>
            </tr>
            <tr>
                <td>Model File</td>
                <td><input type="File" name="itbh-katt-model-file" value="<?= get_post_meta($thepostid, 'itbh-katt-model-file', true)?>"></td>
            </tr>
        </table>
        <?php
    }

    /**
     * Save meta box data.
     */
    public static function save( $post_id, $post ) {
        global $wpdb;

        global $logger;
        $logger->addInfo( 'save : ' );
        // Update post meta
        if ( isset( $_POST['itbh-katt-model-name'] ) ) {
            update_post_meta( $post_id, 'itbh-katt-model-name', itbh_clean( $_POST['itbh-katt-model-name'] ) );
        }

        if ( isset( $_POST['itbh-katt-model-file'] ) ) {
            update_post_meta( $post_id, 'itbh-katt-model-file', itbh_clean( $_POST['itbh-katt-model-file'] ) );
        }

    }

}

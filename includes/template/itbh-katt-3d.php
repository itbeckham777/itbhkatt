<?php
/*
 * Template Name: Good to Be Bad
 * Description: A Page Template with a darker design.
 */
include_once(itbh_katt()->dir . '/vendor/autoload.php');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

// Create the logger
global $logger;
$logger = new Logger('my_logger');
// Now add some handlers
$logger->pushHandler(new StreamHandler('c:/logs/out.log', Logger::DEBUG));
$logger->pushHandler(new FirePHPHandler());

get_header();

global $page_id;
global $paged;
global $itbh_katt;

$hide_title = get_field('mpc_hide_title');

if (function_exists('is_account_page') && is_account_page()) {
    $url = $_SERVER['REQUEST_URI'];

    if (strpos($url, 'edit-address') !== false)
        $hide_title = true;
}

?>
    <div id="mpcth_main">
        <div id="mpcth_main_container">
            <!--            --><?php //get_sidebar(); ?>
            <div id="mpcth_content_wrap" style="width: 100%; border-right: none">
                <div id="mpcth_content">
                    <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <article id="page-<?php the_ID(); ?>" <?php post_class('mpcth-page'); ?> >
                                <?php if (! $hide_title) { ?>
                                    <header class="mpcth-page-header">
                                        <?php
                                        if (function_exists('is_checkout') && is_checkout()) {
                                            $order_url = $_SERVER['REQUEST_URI'];
                                            $order_received = strpos($order_url, '/order-received');
                                            ?>
                                            <div class="mpcth-order-path">
                                                <span><?php _e('Shopping Cart', 'mpcth'); ?></span>
                                                <i class="fa fa-angle-right"></i>
                                                <span <?php echo ! $order_received ? 'class="mpcth-color-main-color"' : ''; ?>><?php _e('Checkout Details', 'mpcth'); ?></span>
                                                <i class="fa fa-angle-right"></i>
                                                <span <?php echo $order_received ? 'class="mpcth-color-main-color"' : ''; ?>><?php _e('Order Complete', 'mpcth'); ?></span>
                                            </div>
                                        <?php }
                                        ?>
                                        <?php mpcth_breadcrumbs(); ?>
                                        <h1 class="mpcth-page-title mpcth-deco-header">
									<span class="mpcth-color-main-border">
										<?php the_title(); ?>
									</span>
                                        </h1>
                                    </header>
                                <?php } ?>
                                <section class="mpcth-page-content itbh-katt-wrap">
                                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                                    <div class="katt-left">
                                        <div id="accordian">
                                            <ul>
                                        <?php
                                        $cats = get_terms( array(
                                            'taxonomy' => 'itbh_3d_cat',
                                            'hide_empty' => false,
                                            'orderby' => 'term_id',
                                            'order' => 'ASC'
                                        ) );
                                        global $logger;

                                        $class_str = 'active';
                                        foreach( $cats as $cat ){
                                            echo '<li class="' . $class_str . '">';
                                            echo '<h3 data-type="things" data-value="' . $cat->slug . '"><i class="fa fa-sort-desc"></i>' . $cat->name . '</h3>';
                                            $logger->addInfo( 'cat = ', array($cat) );
                                            $args = array(
                                                'post_type' => 'itbh_3d_model',
                                                'orderby' => 'ID',
                                                'order'   => 'ASC',
                                                'tax_query' => array(
                                                    array(
                                                        'taxonomy' => 'itbh_3d_cat',
                                                        'field' => 'id',
                                                        'terms' => $cat->term_id
                                                    )
                                                )
                                            );

                                            $the_query = new WP_Query( $args );
                                            if ( $the_query->have_posts() ) {
                                                echo '<ul>';
                                                while ( $the_query->have_posts() ) {
                                                    $the_query->the_post();
                                                    $model_file = get_post_meta( get_the_ID(), 'itbh_katt_model_file', true );
                                                    $model_image = get_post_meta( get_the_ID(), 'itbh_katt_model_image', true );

                                                    echo '<li class="itbh-kit-elem" data-type="' . $cat->slug . '" data-value="' . get_post_field( 'post_name', get_post() ) . '"><a>' . get_the_title() . '</a></li>';
                                                }
                                                echo '</ul>';
                                                wp_reset_postdata();
                                            } else {
                                            }
                                            $class_str = '';
                                        }
                                        ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="katt-middle">
                                        <script src="<?= plugin_dir_url(__FILE__); ?>/3d/main.js"></script>
                                        <iframe src="<?= plugin_dir_url(__FILE__) ;?>/3d/scene.html" width="100%" height="425" id="sd_scene" style="border:1px solid #ddd"></iframe>

                                        <div id="itbh-camera-control">
                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_top_2d_view" style="margin-left: 0px;"></div>
<!--                                            <div class="itbh-cam-button-base itbh-cam-button to_init cam_button_sprite_on" id="cam_flattoon" style="margin-left: 0px;"></div>-->
                                            <div class="itbh-cam-button-base itbh-cam-button to_init cam_button_sprite_on" id="cam_front_3d_view" style="margin-left: 0px;"></div>
<!--                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_zoom_on_selection" style="margin-left: 0px;"></div>-->
                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_zoom_in" style="margin-left: 0px;"></div>
                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_zoom_out" style="margin-left: 0px;"></div>
<!--                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_home" style="margin-left: 0px;"></div>-->
                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_top_view" style="margin-left: 0px;"></div>
                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_orbit_left" style="margin-left: 0px;"></div>
                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_orbit_right" style="margin-left: 0px;"></div>
                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_orbit_up" style="margin-left: 0px;"></div>
                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_orbit_down" style="margin-left: 0px;"></div>
<!--                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_collision" style="margin-left: 0px;"></div>-->
<!--                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_open_item" style="margin-left: 0px;"></div>-->
<!--                                            <div class="itbh-cam-button-base itbh-cam-button to_init" id="cam_delete" style="margin-left: 0px;"></div>-->
                                        </div>
                                        <div class="itbh-color-section">
                                            <div class="itbh-bottom-section">
                                                <a class="itbh-color-elem" data-type="floor" data-value="wood1"><img src="<?= plugin_dir_url(__FILE__) ;?>3d/textures/itbh/floor/wood1.jpg" width="50" height="50"></a>
                                                <a class="itbh-color-elem" data-type="floor" data-value="wood2"><img src="<?= plugin_dir_url(__FILE__) ;?>3d/textures/itbh/floor/wood2.jpg" width="50" height="50"></a>
                                                <a class="itbh-color-elem" data-type="floor" data-value="wood3"><img src="<?= plugin_dir_url(__FILE__) ;?>3d/textures/itbh/floor/wood3.jpg" width="50" height="50"></a><br/>
                                                <a class="itbh-color-elem" data-type="floor" data-value="fabric1"><img src="<?= plugin_dir_url(__FILE__) ;?>3d/textures/itbh/floor/fabric1.jpg" width="50" height="50"></a>
                                                <a class="itbh-color-elem" data-type="floor" data-value="tile1"><img src="<?= plugin_dir_url(__FILE__) ;?>3d/textures/itbh/floor/tile1.jpg" width="50" height="50"></a>
                                                <a class="itbh-color-elem" data-type="floor" data-value="wood1"><img src="<?= plugin_dir_url(__FILE__) ;?>3d/textures/itbh/floor/wood1.jpg" width="50" height="50"></a>
                                            </div>
                                            <div class="itbh-wall-section">
                                                <a class="itbh-color-elem" style="background: #e7e7e7" data-type="wall" data-value="color1"></a>
                                                <a class="itbh-color-elem" style="background: #e7dddb" data-type="wall" data-value="color2"></a>
                                                <a class="itbh-color-elem" style="background: #90cbc5" data-type="wall" data-value="color3"></a><br/>
                                                <a class="itbh-color-elem" style="background: #e6e7e9" data-type="wall" data-value="color4"></a>
                                                <a class="itbh-color-elem" style="background: #e6cac6" data-type="wall" data-value="color5"></a>
                                                <a class="itbh-color-elem" style="background: #907f75" data-type="wall" data-value="color6"></a>
                                            </div>
                                            <div class="itbh-furniture-section">
                                                <a class="itbh-color-elem" style="background: #e7e7e7" data-type="furniture" data-value="furniture1"></a>
                                                <a class="itbh-color-elem" style="background: #e7dddb" data-type="furniture" data-value="furniture2"></a>
                                                <a class="itbh-color-elem" style="background: #90cbc5" data-type="furniture" data-value="furniture3"></a>
                                                <a class="itbh-color-elem" style="background: #e6cac6" data-type="furniture" data-value="furniture4"></a>
                                                <a class="itbh-color-elem" style="background: #907f75" data-type="furniture" data-value="furniture5"></a><br/>
                                                <a class="itbh-color-elem" style="background: #e7e7e7" data-type="furniture" data-value="furniture6"></a>
                                                <a class="itbh-color-elem" style="background: #e7dddb" data-type="furniture" data-value="furniture7"></a>
                                                <a class="itbh-color-elem" style="background: #90cbc5" data-type="furniture" data-value="furniture8"></a>
                                                <a class="itbh-color-elem" style="background: #e6cac6" data-type="furniture" data-value="furniture9"></a>
                                                <a class="itbh-color-elem" style="background: #907f75" data-type="furniture" data-value="furniture10"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="katt-right">

                                    </div>
                                    <div style="clear: both"></div>

                                </section>

                            </article>

                        <?php endwhile; ?>
                    <?php endif; ?>
                </div><!-- end #mpcth_content -->
            </div><!-- end #mpcth_content_wrap -->
        </div><!-- end #mpcth_main_container -->
    </div><!-- end #mpcth_main -->

<?php get_footer();
jQuery(document).ready(function(){
    jQuery("#accordian h3").click(function(){
        //slide up all the link lists
        jQuery("#accordian ul ul").slideUp();
        //slide down the link list below the h3 clicked - only if its closed
        if(!jQuery(this).next().is(":visible"))
        {
            jQuery(this).next().slideDown();
        }
    })

    jQuery('.itbh-kit-elem').click(function(){
        var iframe = document.getElementById('sd_scene').contentWindow;
        var type = jQuery(this).data('type');
        var value = jQuery(this).data('value');

        if( typeof type )

        var message = {
            type: type,
            value: value
        };
        iframe.postMessage(JSON.stringify(message), '*');
    });

    jQuery('.itbh-color-elem').click(function(){
        var iframe = document.getElementById('sd_scene').contentWindow;
        var type = jQuery(this).data('type');
        var value = jQuery(this).data('value');

        var message = {
            type: type,
            value: value
        }
        iframe.postMessage(JSON.stringify(message), '*');
    });

    jQuery('.itbh-cam-button').click(function(){
        var iframe = document.getElementById('sd_scene').contentWindow;

        var value = jQuery(this).attr('id');
        var message = {
            type: 'camera',
            value: value
        }
        iframe.postMessage(JSON.stringify(message), '*');
    });

    jQuery('#cam_top_2d_view').click(function(){
        jQuery(this).addClass('cam_button_sprite_on');
        jQuery('#cam_flattoon').removeClass('cam_button_sprite_on');
        jQuery('#cam_front_3d_view').removeClass('cam_button_sprite_on');
    });
    jQuery('#cam_flattoon').click(function(){
        jQuery(this).addClass('cam_button_sprite_on');
        jQuery('#cam_top_2d_view').removeClass('cam_button_sprite_on');
        jQuery('#cam_front_3d_view').removeClass('cam_button_sprite_on');
    });
    jQuery('#cam_front_3d_view').click(function(){
        jQuery(this).addClass('cam_button_sprite_on');
        jQuery('#cam_top_2d_view').removeClass('cam_button_sprite_on');
        jQuery('#cam_flattoon').removeClass('cam_button_sprite_on');
    });
});

function receiveMessage(event){
    //var source = event.source.frameElement; //this is the iframe that sent the message
    var message = event.data; //this is the message

    message = JSON.parse(message);
    if( message.type == 'floor' ){
        changeFloor( message.value );
    } else if( message.type == 'wall' ){
        changeWall( message.value );
    } else if( message.type == 'furniture' ){
        changeFurniture( message.value );
    } else if( message.type == 'camera' ){
        changeCamera( message.value );
    } else if( message.type == 'lonely-kube' ){
        changeCamera( message.value );
    } else if( message.type == 'scrather' ){
        changeCamera( message.value );
    } else if( message.type == 'toy' ){
        changeCamera( message.value );
    }
}

if( window.addEventListener ){
    window.addEventListener( 'message', receiveMessage, false );
} else {
    window.attachEvent( 'onmessage', receiveMessage );
}


function changeFloor( floor ){
    alert( 'floor : ' + floor );
}

function changeWall( wall ){
    alert( 'wall : ' + wall );
}

function changeFurniture( furniture ){
    alert( 'furniture : ' + furniture );
}

function changeCamera( mode ){
    alert( 'camera : ' + mode );
}